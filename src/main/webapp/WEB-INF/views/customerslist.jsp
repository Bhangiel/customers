<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Customers List</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>

<ul class="tab-navbar">
    <li><a href="javascript:void(0)" class="tablink" onclick="openTab(event, 'CustomersTab');"><span class="lead">Customers</span></a>
    </li>
    <li><a href="javascript:void(0)" class="tablink" onclick="openTab(event, 'CitiesTab');"> <span
            class="lead">Cities</span></a></li>
</ul>


<div id="CustomersTab" class=" tab-container tab">
    <div class="generic-container">
        <div class="panel-heading"><span class="lead">List of Customers</span></div>
        <div class="panel panel-default">

            <!-- Default panel contents -->
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Firstname</th>
                    <th>City</th>
                    <th width="100"></th>
                    <th width="100"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${customers}" var="customer">
                    <tr>
                        <td>${customer.firstName}</td>
                        <td>${customer.city.name}</td>
                        <td><a href="<c:url value='/edit-customer-${customer.id}' />"
                               class="btn btn-success custom-width">edit</a></td>
                        <td><a href="<c:url value='/delete-customer-${customer.id}' />"
                               class="btn btn-danger custom-width">delete</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="well">
            <a href="<c:url value='/newcustomer' />">Add New Customer</a>
        </div>
    </div>
</div>

<div id="CitiesTab" class="tab-container tab">
    <div class="generic-container">
        <div class="panel-heading"><span class="lead">List of Cities</span></div>
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>City</th>
                    <th width="100"></th>
                    <th width="100"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${cityList}" var="city">
                    <tr>
                        <td>${city.name}</td>
                        <th width="100"></th>
                        <th width="100"></th>
                        <td><a href="<c:url value='customers-city-${city.id}' />"
                               class="btn btn-success">show customers</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</div>
</body>
<script>
    openTab(event, 'CustomersTab');
    function openTab(evt, tabName) {
        var i, x, tablinks;
        x = document.getElementsByClassName("tab");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" gray", "");
        }
        document.getElementById(tabName).style.display = "block";
        evt.currentTarget.className += " gray";
    }
</script>

</html>