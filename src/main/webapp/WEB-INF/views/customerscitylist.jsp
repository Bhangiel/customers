<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Customers List</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>

<div class="generic-container">
    <div class="panel-heading"><span class="lead">List of Customers in ${cityname}</span></div>
    <div class="panel panel-default">

        <!-- Default panel contents -->
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Firstname</th>
                <th width="100"></th>
                <th width="100"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${customers}" var="customer">
                <tr>
                    <td>${customer.firstName}</td>
                    <td>${customer.city.name}</td>
                    <td><a href="<c:url value='/edit-customer-${customer.id}' />"
                           class="btn btn-success custom-width">edit</a></td>
                    <td><a href="<c:url value='/delete-customer-${customer.id}' />"
                           class="btn btn-danger custom-width">delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
	<span class="well floatRight">
		Go to <a href="<c:url value='/list' />">Cities List</a>
	</span>
</div>
</body>

</html>