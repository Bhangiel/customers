INSERT INTO `city` (`ID_CITY`, `NAME`) VALUES ('1', 'Wroclaw');
INSERT INTO `city` (`ID_CITY`, `NAME`) VALUES ('2', 'Warszawa');
INSERT INTO `city` (`ID_CITY`, `NAME`) VALUES ('3', 'New York');
INSERT INTO `city` (`ID_CITY`, `NAME`) VALUES ('4', 'Barcelona');
INSERT INTO `city` (`ID_CITY`, `NAME`) VALUES ('5', 'Radom');
INSERT INTO `city` (`ID_CITY`, `NAME`) VALUES ('6', 'Zielona Gora');

INSERT INTO `customer` (`ID`, `FIRST_NAME`, `CITY`) VALUES ('1', 'Boleslaw', '1');
INSERT INTO `customer` (`ID`, `FIRST_NAME`, `CITY`) VALUES ('2', 'Andrzej', '2');
INSERT INTO `customer` (`ID`, `FIRST_NAME`, `CITY`) VALUES ('3', 'Tomasz', '1');
INSERT INTO `customer` (`ID`, `FIRST_NAME`, `CITY`) VALUES ('4', 'Anna', '5');
INSERT INTO `customer` (`ID`, `FIRST_NAME`, `CITY`) VALUES ('5', 'Urszula', '4');
INSERT INTO `customer` (`ID`, `FIRST_NAME`, `CITY`) VALUES ('6', 'Krzysztof', '6');