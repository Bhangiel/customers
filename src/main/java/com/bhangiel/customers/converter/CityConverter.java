package com.bhangiel.customers.converter;

import com.bhangiel.customers.model.City;
import com.bhangiel.customers.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class CityConverter implements Converter<Object, City> {

    @Autowired
    CityService cityService;

    public City convert(Object element) {
        Integer id = Integer.parseInt((String) element);
        City city = cityService.findById(id);
        System.out.println("City : " + city);
        return city;
    }

}