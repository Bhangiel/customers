package com.bhangiel.customers.service;

import com.bhangiel.customers.model.Customer;

import java.util.List;


public interface CustomerService {

    Customer findById(int id);

    void saveCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomerById(int id);

    List<Customer> findAllCustomers();

    List<Customer> findCustomersByCityId(int cityId);


}