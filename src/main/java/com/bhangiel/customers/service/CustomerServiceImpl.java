package com.bhangiel.customers.service;

import com.bhangiel.customers.dao.CustomerDao;
import com.bhangiel.customers.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("CustomerService")
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao dao;

    public Customer findById(int id) {
        return dao.findById(id);
    }


    public void saveCustomer(Customer customer) {
        dao.save(customer);
    }

    public void updateCustomer(Customer customer) {
        Customer entity = dao.findById(customer.getId());
        if (entity != null) {
            entity.setFirstName(customer.getFirstName());
            entity.setCity(customer.getCity());

        }
    }


    public void deleteCustomerById(int id) {
        dao.deleteById(id);
    }

    public List<Customer> findAllCustomers() {
        return dao.findAllCustomers();
    }

    public List<Customer> findCustomersByCityId(int cityId) {
        return dao.findCustomersByCityId(cityId);
    }

}
