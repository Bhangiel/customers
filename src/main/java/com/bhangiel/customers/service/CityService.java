package com.bhangiel.customers.service;

import com.bhangiel.customers.model.City;

import java.util.List;


public interface CityService {

    City findById(int id);

    void saveCity(City city);

    void updateCity(City city);

    void deleteCityById(int id);

    List<City> findAllCities();


}