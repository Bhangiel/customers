package com.bhangiel.customers.service;

import com.bhangiel.customers.dao.CityDao;
import com.bhangiel.customers.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("CityService")
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao dao;

    public City findById(int id) {
        return dao.findById(id);
    }


    public void saveCity(City city) {
        dao.save(city);
    }

    public void updateCity(City city) {
        City entity = dao.findById(city.getId());
        if (entity != null) {
            entity.setName(city.getName());
        }
    }


    public void deleteCityById(int id) {
        dao.deleteById(id);
    }

    public List<City> findAllCities() {
        return dao.findAllCities();
    }


}
