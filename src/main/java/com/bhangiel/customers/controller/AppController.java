package com.bhangiel.customers.controller;

import com.bhangiel.customers.model.City;
import com.bhangiel.customers.model.Customer;
import com.bhangiel.customers.service.CityService;
import com.bhangiel.customers.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/")
public class AppController {

    @Autowired
    CustomerService customerService;
    @Autowired
    CityService cityService;
    @Autowired
    MessageSource messageSource;

    /**
     * List of all existing customers.
     */
    @RequestMapping(value = {"/", "/list"}, method = RequestMethod.GET)
    public String listCustomers(ModelMap model) {

        List<Customer> customers = customerService.findAllCustomers();
        model.addAttribute("customers", customers);
        return "customerslist";
    }

    /**
     * Adding new customer
     */
    @RequestMapping(value = {"/newcustomer"}, method = RequestMethod.GET)
    public String newUser(ModelMap model) {
        Customer customer = new Customer();
        model.addAttribute("customer", customer);
        model.addAttribute("edit", false);
        return "creation";
    }

    /**
     * POST Handler customer save
     */
    @RequestMapping(value = {"/newcustomer"}, method = RequestMethod.POST)
    public String saveCustomer(@Valid Customer customer, BindingResult result,
                               ModelMap model) {

        if (result.hasErrors()) {
            return "creation";
        }

        customerService.saveCustomer(customer);

        model.addAttribute("success", "Customer " + customer.getFirstName() + " created successfully");
        //return "success";
        return "creationsuccess";
    }

    /**
     * Updating customer
     */

    @RequestMapping(value = {"/edit-customer-{id}"}, method = RequestMethod.GET)
    public String editUser(@PathVariable int id, ModelMap model) {
        Customer customer = customerService.findById(id);
        model.addAttribute("customer", customer);
        model.addAttribute("cityname", customer.getCity().getName());
        model.addAttribute("edit", true);
        return "creation";
    }

    /**
     * POST Handler for customer update
     */
    @RequestMapping(value = {"/edit-customer-{id}"}, method = RequestMethod.POST)
    public String updateUser(@Valid Customer customer, BindingResult result,
                             ModelMap model) {

        if (result.hasErrors()) {
            return "creation";
        }
        customerService.updateCustomer(customer);

        model.addAttribute("success", "Customer " + customer.getFirstName() + " updated successfully");
        return "creationsuccess";
    }

    /**
     * Deleting customer by id
     */
    @RequestMapping(value = {"/delete-customer-{id}"}, method = RequestMethod.GET)
    public String deleteUser(@PathVariable int id) {
        customerService.deleteCustomerById(id);
        return "redirect:/list";
    }

    /**
     * List of all customer from selected city
     */

    @RequestMapping(value = {"/customers-city-{id}"}, method = RequestMethod.GET)
    public String showCustomers(@PathVariable int id, ModelMap model) {
        List<Customer> customers = customerService.findCustomersByCityId(id);
        City city = cityService.findById(id);
        model.addAttribute("customers", customers);
        model.addAttribute("cityname", city.getName());
        return "customerscitylist";
    }

    /**
     * List of all cities
     */
    @ModelAttribute("cityList")
    public List<City> initializeCities() {
        return cityService.findAllCities();
    }

}
