package com.bhangiel.customers.dao;

import com.bhangiel.customers.model.City;

import java.util.List;


public interface CityDao {

    City findById(int id);

    void save(City city);

    void deleteById(int id);

    List<City> findAllCities();

}