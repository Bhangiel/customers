package com.bhangiel.customers.dao;

import com.bhangiel.customers.model.Customer;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("CustomerDao")
public class CustomerDaoImpl extends AbstractDao<Integer, Customer> implements CustomerDao {

    public Customer findById(int id) {
        Customer customer = getByKey(id);
        return customer;
    }

    @SuppressWarnings("unchecked")
    public List<Customer> findAllCustomers() {
        Criteria criteria = createEntityCriteria().addOrder(Order.asc("firstName"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Customer> customers = (List<Customer>) criteria.list();

        return customers;
    }

    @SuppressWarnings("unchecked")
    public List<Customer> findCustomersByCityId(int cityId) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("city.id", cityId));
        criteria.addOrder(Order.asc("firstName"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Customer> customers = (List<Customer>) criteria.list();

        return customers;
    }

    public void save(Customer customer) {
        persist(customer);
    }

    public void deleteById(int id) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("id", id));
        Customer customer = (Customer) crit.uniqueResult();
        delete(customer);
    }

}
