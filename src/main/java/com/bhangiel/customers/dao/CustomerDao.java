package com.bhangiel.customers.dao;

import com.bhangiel.customers.model.Customer;

import java.util.List;


public interface CustomerDao {

    Customer findById(int id);

    void save(Customer customer);

    void deleteById(int id);

    List<Customer> findAllCustomers();

    List<Customer> findCustomersByCityId(int cityId);
}

