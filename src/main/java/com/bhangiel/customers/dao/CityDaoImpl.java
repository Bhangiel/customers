package com.bhangiel.customers.dao;

import com.bhangiel.customers.model.City;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("CityDao")
public class CityDaoImpl extends AbstractDao<Integer, City> implements CityDao {

    public City findById(int id) {
        City city = getByKey(id);
        return city;
    }

    @SuppressWarnings("unchecked")
    public List<City> findAllCities() {
        Criteria crit = createEntityCriteria();
        crit.addOrder(Order.asc("name"));
        crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return (List<City>) crit.list();
    }

    public void save(City city) {
        persist(city);
    }

    public void deleteById(int id) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("id", id));
        City city = (City) crit.uniqueResult();
        delete(city);
    }

}
